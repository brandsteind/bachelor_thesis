#include "allocator.h"
#include <stdio.h>

unsigned char memory[ALLOCATOR_MEMPOOL_SIZE];
static size_t memory_ptr;

bool check_mem_address(void* addr)
{
  void* m = (void*) memory;
  return m <= addr && addr < (m + ALLOCATOR_MEMPOOL_SIZE);
}

void* allocate(size_t size)
{
  ASSERT(memory_ptr + size < ALLOCATOR_MEMPOOL_SIZE);
  void* ret = &memory[memory_ptr];
  memory_ptr += size;
  return ret;
}

void* fake_malloc(size_t size)
{
#ifdef DEBUG_TRACE
  fprintf(stderr, "fake_malloc\n");
#endif
  return allocate(size);
}

void fake_free(void* ptr)
{
  (void)ptr;
#ifdef DEBUG_TRACE
  fprintf(stderr, "fake_free\n");
#endif
  ASSERT(check_mem_address(ptr));
}

void* fake_calloc(size_t nmemb, size_t size)
{
/* TODO: check for overflows */
  size_t block_size = nmemb * size;
  void* ret;
#ifdef DEBUG_TRACE
  fprintf(stderr, "fake_calloc\n");
#endif
  ret = allocate(block_size);
  bzero(ret, block_size);
  return ret;
}

void* fake_realloc(void* ptr, size_t size)
{
#ifdef DEBUG_TRACE
  fprintf(stderr, "fake_realloc\n");
#endif
  ASSERT(ptr == NULL || check_mem_address(ptr));
  void* ret = allocate(size);
  if (ptr)
  {
    memmove(ret, ptr, size);
    fake_free(ptr);
  }
  return ret;
}
