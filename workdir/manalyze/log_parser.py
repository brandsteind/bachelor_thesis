# Hierarchy:
#     app:
#         - process(id):
#             - thread(id):
#                 - call:
#                     info:
#                         pid, tid, cpu_time
#                     name: string
#                     params:
#                         - param_string

import re
import yaml

class CallInfo:
    def __init__(self, cpu_time, wall_time):
        self.cpu_time_start = cpu_time
        self.cpu_time_ret = None
        self.wall_time_start = wall_time
        self.wall_time_ret = None

    def __repr__(self):
        return str(f"cpu_time_start: {self.cpu_time_start}, cpu_time_ret: {self.cpu_time_ret}, "
                   f"wall_time_start: {self.wall_time_start}, wall_time_ret: {self.wall_time_ret}")

class Call:
    def __init__(self, info, name):
        self.info = info
        self.name = name
        self.params = None
        self.ret = None

    def __repr__(self):
        return f"\nCall:\n\tname: {self.name}\n\tinfo: {self.info}\n\tparams: {self.params}\n\tret: {self.ret}\n"

    def add_parameters(self, params, ret):
        self.params = params
        self.ret = ret

def parse_line(line, trace, for_analysis):
    info_re = "\(pid (?P<pid>.*)\) \(tid (?P<tid>.*)\) \[(?P<cpu_time>.*)\].*\{(?P<wall_time>.*)\} (?P<rest>.*)"
    match = re.match(info_re, line)
    assert match
    pid, tid, cpu_time, wall_time, rest = match.groups()
    cpu_time = float(cpu_time)
    wall_time = float(wall_time)
    # print(f"PID: {pid}; TID: {tid}; cpu_time: {cpu_time}")
    # print(rest)
    tp = trace.get(pid)
    if tp is None:
        trace[pid] = dict()
    tt = trace[pid].get(tid)
    if tt is None:
        trace[pid][tid] = list()

    call_re = "call (.*)"
    call_match = re.match(call_re, rest)
    if call_match:
        name = call_match.groups()[0]
        if for_analysis:
            if name != "pthread_create":
                trace[pid][tid].append(Call(CallInfo(cpu_time, wall_time), name))
        else:
            if (len(trace[pid][tid]) == 0
                or
                trace[pid][tid][-1].name != "pthread_create"
                or
                trace[pid][tid][-1].name == "pthread_create" and
                trace[pid][tid][-1].params is not None):
                trace[pid][tid].append(Call(CallInfo(cpu_time, wall_time), name))

    else:
        params_str = rest.split(' -> ')
        if len(params_str) > 1:
            call, ret = params_str
        else:
            call, ret = params_str[0], None
        params_match = re.match("(?P<name>.*?)\((?P<params>.*)\)", call)
        assert params_match
        name, params = params_match.groups()
        if (for_analysis and name != "pthread_create"
            or
            not for_analysis and
            (trace[pid][tid][-1].name != "pthread_create"
             or name == "pthread_create")):
            params = params.split(', ')
            last_call = trace[pid][tid][-1]
            assert last_call.name == name
            last_call.add_parameters(params, ret)
            last_call.info.cpu_time_ret = cpu_time
            last_call.info.wall_time_ret = wall_time


def parse_file(filename, for_analysis = False):
    d = dict()
    with open(filename) as file:
        for line in file:
            parse_line(line, d, for_analysis)
    # print(d)
    # yaml_d = yaml.dump(d)
    # print(yaml_d)
    return d


if __name__ == "__main__":
   import sys
   print(parse_file(sys.argv[1]))
