#include <stdlib.h>

#include <pthread.h>

#define THREAD_NUM 10
pthread_t threads[THREAD_NUM];

void* thread_main(void* arg)
{
  int thread_num = (int)arg;
  void* p = malloc(thread_num * 10);
  free(p);
  p = malloc(thread_num * 20);
  free(p);
  p = malloc(thread_num * 30);
  free(p);
}

int main()
{
  for (int i = 0; i < THREAD_NUM; i++)
  {
    pthread_create(&threads[i], NULL, thread_main, (void*) (i + 1));
  }

  for (int i = 0; i < THREAD_NUM; i++)
  {
    pthread_join(threads[i], NULL);
  }
  return 0;
}
