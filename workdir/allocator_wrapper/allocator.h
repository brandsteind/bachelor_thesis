#ifndef ALLOCATOR_H
#define ALLOCATOR_H

#include <stddef.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#include "config.h"
#include "errors.h"
#include "assert.h"


bool check_mem_address(void* addr);

void* fake_malloc(size_t size);
void fake_free(void* ptr);
void* fake_calloc(size_t nmemb, size_t size);
void* fake_realloc(void* ptr, size_t size);

#endif
