import click

import codegen
import log_parser

from launcher import main

@main.command()
@click.option('-t', '--preserve-timings', is_flag=True, default = False)
@click.option('-o', '--output-file', type=click.File(mode='w'), default="cloned_main.c")
@click.option('-p', '--pid')
@click.argument('input-log', nargs = 1, type=click.Path(exists=True,
                                                        readable=True,
                                                        dir_okay=False),
                default="decoder.output")
def clone(preserve_timings, input_log, output_file, pid):
    parsed_dict = log_parser.parse_file(input_log)
    builder = codegen.CodeBuilder()
    if pid is not None:
        process_dict = parsed_dict[pid]
    else:
        process_dict = list(parsed_dict.values())[0]
        if len(parsed_dict) > 1:
            click.echo("Cloning is not supported for multiprocess apps")
            return
    output_file.write(builder.gen(process_dict, preserve_timings))
