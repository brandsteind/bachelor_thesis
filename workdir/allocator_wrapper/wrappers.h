#ifndef WRAPPERS_H
#define WRAPPERS_H

#include <stdlib.h>
#include <stdio.h>
#include <dlfcn.h>
#include <pthread.h>
/* replace it with custom functions */
#include <string.h>

#include "errors.h"
#include "assert.h"

#define DECLARE_FUNC(funcname)						\
  static funcname ## _t real_ ## funcname = NULL;			\
  static pthread_mutex_t funcname ## _mutex = PTHREAD_MUTEX_INITIALIZER;

#define INIT_FUNC(funcname, module)					\
  __attribute__((constructor(200)))					\
  static void init_ ## funcname ()					\
  {									\
    Dl_info dl_info;							\
    pthread_mutex_lock(& funcname ## _mutex);				\
    if (real_ ## funcname != NULL)					\
    {									\
      return;								\
    }									\
    real_ ## funcname = ( funcname ## _t )dlsym(RTLD_NEXT, #funcname);	\
    ASSERT(real_ ## funcname);						\
    dladdr(real_ ##funcname, &dl_info);					\
    fprintf(stderr, #funcname " from %s\n", dl_info.dli_fname);		\
    pthread_mutex_unlock(& funcname ## _mutex);				\
  }


#endif
