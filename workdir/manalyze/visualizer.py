import click
import matplotlib.pyplot as plt
import numpy as np

from launcher import main
import log_parser


def show_plot(malloc, calloc, realloc, free):
    objects = ('malloc', 'calloc', 'realloc', 'free', 'total')
    y_pos = np.arange(len(objects))
    total_time = malloc + calloc + realloc + free
    time = [malloc, calloc, realloc, free, total_time]

    plt.bar(y_pos, time, align='center', alpha=0.5)
    plt.xticks(y_pos, objects)
    plt.ylabel('Time')

    plt.show()


@main.command()
@click.argument('input-log', nargs = 1, type=click.Path(exists=True,
                                                        readable=True,
                                                        dir_okay=False),
                default="decoder.output")
def visualize(input_log):
    parsed_dict = log_parser.parse_file(input_log, True)
    times = { 'malloc' : 0,
              'calloc' : 0,
              'realloc' : 0,
              'free' : 0 }
    for process in parsed_dict:
        for thread in parsed_dict[process]:
            for call in parsed_dict[process][thread]:
                times[call.name] += call.info.cpu_time_ret - call.info.cpu_time_start

    show_plot(*times.values())
