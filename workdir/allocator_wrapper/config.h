#ifndef CONFIG_H
#define CONFIG_H


#define MALLOC_LIBRARY "/usr/lib/libc.so.6"
/* #define MALLOC_LIBRARY "/usr/lib/libjemalloc.so" */
#define PTHREAD_LIBRARY "/usr/lib/libpthread.so"

#define WRAPPER_LIBRARY "/home/brandstein/vsu/diploma/workdir/utils/allocator_wrapper/allocation_tracer.so"
/* #define ENABLE_PACKED_STRUCTS */

#define ALLOCATOR_MEMPOOL_SIZE 1024 * 1024

#define SYS_PAGE_SIZE 4096

#define ASSERT_FILENAME "assert.log"

/* #define DEBUG_TRACE */

#endif
