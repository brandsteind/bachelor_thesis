#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <limits.h>

#include <unistd.h>
#include <errno.h>
#include <sys/wait.h>
#include <sys/ptrace.h>
#include <sys/reg.h>
#include <sys/user.h>
#include <asm/unistd_64.h>

#include "tracer.h"
#include "call_trace.h"

char* env_trace_syscall;
char tracer_dso_path[PATH_MAX];

void prepare_env()
{
  /* FIXME: check for buffer overrun */
  char ld_preload_new[1024];
  ld_preload_new[0] = 0;
  strcat(ld_preload_new, tracer_dso_path);
  char* ld_preload_old = getenv("LD_PRELOAD");
  if (ld_preload_old)
  {
    strcat(ld_preload_new, " ");
    strcat(ld_preload_new, ld_preload_old);
  }
  if(setenv("LD_PRELOAD", ld_preload_new, 1))
  {
    perror("failed to prepare environment");
    exit(0);
  }
}

long long rdi, rsi, rdx, r10, r8, r9;

void handle_syscall_enter(pid_t tid, long long rax)
{
  rdi = ptrace(PTRACE_PEEKUSER, tid, 8 * RDI, NULL);
  rsi = ptrace(PTRACE_PEEKUSER, tid, 8 * RSI, NULL);
  rdx = ptrace(PTRACE_PEEKUSER, tid, 8 * RDX, NULL);
  r10 = ptrace(PTRACE_PEEKUSER, tid, 8 * R10, NULL);
  r8 = ptrace(PTRACE_PEEKUSER, tid, 8 * R8, NULL);
  r9 = ptrace(PTRACE_PEEKUSER, tid, 8 * R9, NULL);

  switch(rax)
  {
  case __NR_brk:
    trace_entry(CALL_TYPE_BRK);
    break;
  case __NR_mmap:
    trace_entry(CALL_TYPE_MMAP);
    break;
  case __NR_munmap:
    trace_entry(CALL_TYPE_MUNMAP);
    break;
  }

  /* printf("child %d, syscall %lld\n", tid, rax); */
  /* printf("args: %lld, %lld, %lld, %lld, %lld, %lld\n", */
  /* 	 rdi, rsi, rdx, r10, r8, r9); */
}

void handle_syscall_exit(pid_t tid, long long orig_rax)
{
  long long rax;
  rax = ptrace(PTRACE_PEEKUSER, tid, 8 * RAX, NULL);
  switch (orig_rax)
  {
  case __NR_brk:
    trace_brk((void*) rdi, (void*) rax);
    break;
  case __NR_mmap:
    trace_mmap((void*) rdi, (size_t) rsi, (int) rdx,
	       (int) r10, (int) r8, (off_t) r9, (void*) rax);
    break;
  case __NR_munmap:
    trace_munmap((void*)rdi, (size_t) rsi, (int) rax);
    break;
  }
  /* printf("ret %lld\n", rax); */
}

int main (int argc, char **argv)
{
  env_trace_syscall = getenv(TRACER_ENV_TRACE_SYSCALL);
  bool syscall_trace_enabled = !!env_trace_syscall;

  if (realpath(TRACER_DSO, tracer_dso_path) == NULL)
  {
    perror("failed to resolve TRACER_DSO path");
    return errno;
  }

  prepare_env();

  int child_pid;
  if (argc == 1)
  {
    printf("Command is not specified\n");
    return 0;
  }

  if ((child_pid = fork()))
  {
    int stat_val;
    pid_t tid;
    bool is_inside = true;
    bool first_time = true;
    while((tid = wait(&stat_val)))
    {
      if (syscall_trace_enabled)
      {
	if (WSTOPSIG(stat_val) == SIGTRAP)
	{
	  long long rax = 0;
	  if (first_time)
	  {
	    first_time = 0;
	    /* ptrace(PTRACE_SETOPTIONS, tid, NULL, PTRACE_O_EXITKILL | PTRACE_O_TRACECLONE); */
	  }
	  rax = ptrace(PTRACE_PEEKUSER, tid, 8 * ORIG_RAX, NULL);
	  if (!is_inside)
	  {
	    is_inside = true;
	    handle_syscall_enter(tid, rax);
	  }
	  else
	  {
	    is_inside = false;
	    handle_syscall_exit(tid, rax);
	  }
	  ptrace(PTRACE_SYSCALL, child_pid, NULL, NULL);
	}
      }
      if (WIFEXITED(stat_val))
      {
	printf("child %d terminated\n", child_pid);
	return 0;
      }
      if (WIFSIGNALED(stat_val))
      {
	printf("Child exited due to signal %d\n", WTERMSIG(stat_val));
	return 0;
      }
    }
  }
  else
  {
    if (syscall_trace_enabled)
    {
      ptrace(PTRACE_TRACEME, 0, NULL, NULL);
    }
    execv(argv[1], &argv[1]);
    perror("failed to execve");
  }
  return 0;
}
