#define _GNU_SOURCE
#include <stdio.h>

#include <dlfcn.h>
#include <pthread.h>

#include "config.h"
#include "wrappers.h"
#include "call_trace.h"
#include "allocator.h"

typedef void* (*malloc_t) (size_t);
typedef void (*free_t) (void*);
typedef void* (*calloc_t) (size_t, size_t);
typedef void* (*realloc_t) (void*, size_t);
typedef void* (*reallocarray_t) (void*, size_t, size_t);

typedef int (*brk_t) (void*);
typedef void* (*sbrk_t) (intptr_t);
typedef void* (*mmap_t) (void*, size_t, int, int, int, off_t);
typedef int (*munmap_t) (void*, size_t);

typedef int (*pthread_create_t) (pthread_t*, const pthread_attr_t*,
				 void* (*) (void*), void*);

DECLARE_FUNC(malloc);
DECLARE_FUNC(free);
DECLARE_FUNC(calloc);
DECLARE_FUNC(realloc);
DECLARE_FUNC(reallocarray);
DECLARE_FUNC(pthread_create);

INIT_FUNC(malloc, MALLOC_LIBRARY);
INIT_FUNC(free, MALLOC_LIBRARY);
INIT_FUNC(calloc, MALLOC_LIBRARY);
INIT_FUNC(realloc, MALLOC_LIBRARY);
INIT_FUNC(reallocarray, MALLOC_LIBRARY);
INIT_FUNC(pthread_create, PTHREAD_LIBRARY);

static bool is_constructed = false;

__attribute__((constructor(110)))
void init(void)
{
  is_constructed = true;
}

__attribute__((visibility("default")))
void* malloc (size_t size)
{
  static bool malloc_in_init = false;
#ifdef DEBUG_TRACE
  fprintf(stderr, "malloc\n");
#endif
  if (real_malloc == NULL)
  {
    if (!malloc_in_init)
    {
      malloc_in_init = true;
      init_malloc();
    }
    else
    {
      /* we can't get real malloc address as libdl.so use malloc internally;
	 so we need to serve some requests on our own; */
      return fake_malloc(size);
    }
  }
  ASSERT(real_malloc);
  //if (is_constructed)
  {
    trace_entry(CALL_TYPE_MALLOC);
  }
  void* ret = real_malloc(size);
  //if (is_constructed)
  {
    trace_malloc(size, ret);
  }
  return ret;
}

__attribute__((visibility("default")))
void free(void* ptr)
{
#ifdef DEBUG_TRACE
  fprintf(stderr, "free\n");
#endif
  if (ptr == NULL)
    return;
  if (check_mem_address(ptr))
  {
    fake_free(ptr);
    return;
  }
  if (real_free == NULL)
  {
    init_free();
  }
  ASSERT(real_free);
  ASSERT(!check_mem_address(ptr));
  //if (is_constructed)
  {
    trace_entry(CALL_TYPE_FREE);
  }
  real_free(ptr);
  //if (is_constructed)
  {
    trace_free(ptr);
  }
}

__attribute__((visibility("default")))
void* calloc(size_t nmemb, size_t size)
{
  static bool calloc_in_init = false;
#ifdef DEBUG_TRACE
  fprintf(stderr, "calloc\n");
#endif
  if (real_calloc == NULL)
  {
    if (!calloc_in_init)
    {
      calloc_in_init = true;
      init_calloc();
    }
    else
    {
      /* we can't get real calloc address as libdl.so use calloc internally;
	 so we need to serve some requests on our own; */
      return fake_calloc(nmemb, size);
    }
  }
  ASSERT(real_calloc);
  //if (is_constructed)
  {
    trace_entry(CALL_TYPE_CALLOC);
  }
  void* ret = real_calloc(nmemb, size);
  //if (is_constructed)
  {
    trace_calloc(nmemb, size, ret);
  }
  return ret;
}

__attribute__((visibility("default")))
void* realloc(void* ptr, size_t size)
{
#ifdef DEBUG_TRACE
  fprintf(stderr, "realloc\n");
#endif
  if (check_mem_address(ptr))
  {
    return fake_realloc(ptr, size);
  }
  if (real_realloc == NULL)
  {
    init_realloc();
  }
  ASSERT(real_realloc);
  ASSERT(!check_mem_address(ptr));
  //if (is_constructed)
  {
    trace_entry(CALL_TYPE_REALLOC);
  }
  void* ret = real_realloc(ptr, size);
  //if (is_constructed)
  {
    trace_realloc(ptr, size, ret);
  }
  return ret;
}

__attribute__((visibility("default")))
void* reallocarray(void* ptr, size_t nmemb, size_t size)
{
#ifdef DEBUG_TRACE
  fprintf(stderr, "reallocarray\n");
#endif
  ASSERT(real_reallocarray);
  //if (is_constructed)
  {
    trace_entry(CALL_TYPE_REALLOCARRAY);
  }
  void* ret = real_reallocarray(ptr, nmemb, size);
  //if (is_constructed)
  {
    trace_reallocarray(ptr, nmemb, size, ret);
  }
  return ret;
}

__attribute__((visibility("default")))
int pthread_create(pthread_t *thread, const pthread_attr_t *attr,
		   void* (*start_routine) (void*), void *arg)
{
#ifdef DEBUG_TRACE
  fprintf(stderr, "pthread_create\n");
#endif
  ASSERT(real_pthread_create);
  //if (is_constructed)
  {
    trace_entry(CALL_TYPE_PTHREAD_CREATE);
  }
  int ret = real_pthread_create(thread, attr, start_routine, arg);
  //if (is_constructed)
  {
    trace_pthread_create(thread, attr, start_routine, arg, ret);
  }
  return ret;
}
