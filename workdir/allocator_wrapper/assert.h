#ifndef ASSERT_H
#define ASSERT_H

#include <stdlib.h>
#include <stdbool.h>

#include <unistd.h>
#include <fcntl.h>

#include "errors.h"
#include "config.h"

#define STRINGIFY(line) #line
#define FILELINE(file, line) __FILE__ ":" STRINGIFY(line)

void wrapper_assert(bool cond, const char* fileline);

#define ASSERT(cond) wrapper_assert((cond), FILELINE(__FILE__, __LINE__))

#endif
