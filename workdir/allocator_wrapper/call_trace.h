#ifndef CALL_INFO_H
#define CALL_INFO_H

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include <pthread.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/syscall.h>
#include <sys/resource.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>
#include <semaphore.h>

#include "config.h"
#include "errors.h"
#include "assert.h"

#define TRACE_OUTPUT_FILENAME "call_trace.bl"

#define TRACE_CACHE_SIZE 10000

#ifdef ENABLE_PACKED_STRUCTS
#define PACKED_PRE
#define PACKED_POST __attribute__((packed))
#else
#define PACKED_PRE
#define PACKED_POST
#endif

typedef enum call_type_e {
  CALL_TYPE_MALLOC,
  CALL_TYPE_FREE,
  CALL_TYPE_CALLOC,
  CALL_TYPE_REALLOC,
  CALL_TYPE_REALLOCARRAY,
  CALL_TYPE_PTHREAD_CREATE,
  CALL_TYPE_BRK,
  CALL_TYPE_MMAP,
  CALL_TYPE_MUNMAP
} call_type_t;

typedef struct call_trace_hdr_s {
  call_type_t call_type;
  pid_t pid;
  pid_t tid;
  struct timespec time;
  struct timespec wall_time;
  bool has_body; /* if this trace record contains arguments and a return value */
} PACKED_POST call_trace_hdr_t;

typedef struct call_trace_malloc_s {
  size_t size;
  void* ret;
} PACKED_POST call_trace_malloc_t;

typedef struct call_trace_free_s {
  void* ptr;
} PACKED_POST call_trace_free_t;

typedef struct call_trace_calloc_s {
  size_t nmemb;
  size_t size;
  void* ret;
} PACKED_POST call_trace_calloc_t;

typedef struct call_trace_realloc_s {
  void* ptr;
  size_t size;
  void* ret;
} PACKED_POST call_trace_realloc_t;

typedef struct call_trace_reallocarray_s {
  void* ptr;
  size_t nmemb;
  size_t size;
  void* ret;
} PACKED_POST call_trace_reallocarray_t;

typedef struct call_trace_brk_s {
  void* addr;
  void* ret;
} PACKED_POST call_trace_brk_t;

typedef struct call_trace_mmap_s {
  void* addr;
  size_t len;
  int prot;
  int flags;
  int fildes;
  off_t off;
  void* ret;
} PACKED_POST call_trace_mmap_t;

typedef struct call_trace_munmap_s {
  void* addr;
  size_t len;
  int ret;
} PACKED_POST call_trace_munmap_t;

typedef struct call_trace_pthread_create_s {
  int ret;
} PACKED_POST call_trace_pthread_create_t;

typedef struct call_trace_entry_s {
  call_trace_hdr_t hdr;
  union {
    call_trace_malloc_t malloc;
    call_trace_free_t free;
    call_trace_calloc_t calloc;
    call_trace_realloc_t realloc;
    call_trace_reallocarray_t reallocarray;
    call_trace_pthread_create_t pthread_create;
    call_trace_brk_t brk;
    call_trace_mmap_t mmap;
    call_trace_munmap_t munmap;
  } body;
} PACKED_POST call_trace_entry_t;

void trace_flush();
void trace_write(call_trace_hdr_t* hdr, void* trace_body);


void trace_entry(call_type_t call_type);

void trace_malloc(size_t size, void* ret);
void trace_free(void* ptr);
void trace_calloc(size_t nmemb, size_t size, void* ret);
void trace_realloc(void* ptr, size_t size, void* ret);
void trace_reallocarray(void* ptr, size_t nmemb, size_t size, void* ret);
void trace_brk(void* addr, void* ret);
void trace_mmap(void* addr, size_t len, int prot, int flags, int fildes, off_t off, void* ret);
void trace_munmap(void* addr, size_t len, int ret);
void trace_pthread_create(pthread_t *thread, const pthread_attr_t *addr,
			  void* (*start_routine) (void*), void *arg, int ret);

#endif
