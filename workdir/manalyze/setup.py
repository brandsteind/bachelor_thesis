from setuptools import setup

setup(
    name='manalyze',
    version='0.1',
    py_modules=['launcher', 'tracer'],
    install_requires=[
        'Click',
        'pyyaml',
        'matplotlib',
        'numpy'
    ],
    entry_points='''
        [console_scripts]
        manalyze=launcher:main
    ''',
)
