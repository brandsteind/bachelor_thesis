import click
import os
import sys
import subprocess

from pathlib import Path

from launcher import main

default_tracer_path = "allocation_tracer.so"
default_decoder_path="./decoder"
trace_file = "call_trace.bl"

prev_ld_preload = ""

def prepare_environment(allocator, tracer_lib):
    tracer_lib = os.path.realpath(tracer_lib)
    prev_ld_preload= os.environ.get('LD_PRELOAD', '')
    os.environ['LD_PRELOAD'] = tracer_lib
    if allocator:
        allocator = os.path.realpath(allocator)
        os.environ['LD_PRELOAD'] = f"{os.environ['LD_PRELOAD']} {allocator}"

def run_command(command):
    return subprocess.call(command)

def restore_environment():
    os.environ['LD_PRELOAD'] = prev_ld_preload


@main.command()
@click.option('-a', '--allocator', type=click.Path(exists=True,
                                                   readable=True,
                                                   dir_okay=False))
@click.option('-t', '--tracer-lib', type=click.Path(exists=True,
                                                    readable=True,
                                                    dir_okay=False),
              default=default_tracer_path,
              envvar='MANALYZE_TRACER_LIB')
@click.option('-c', '--clear-previous-trace', is_flag=True, default=False)
@click.option('--decode/--no-decode', 'shall_decode', default=True)
@click.option('-d', '--decoder', type=click.Path(exists=True,
                                                 readable=True,
                                                 dir_okay=False),
              default=default_decoder_path,
              envvar='MANALYZE_DECODER')
@click.option('-o', '--decoder-output-file', type=click.File(mode='w'),
              default="decoder.output")
@click.argument('command', nargs=-1, required=True)
def trace(allocator, tracer_lib, command, clear_previous_trace, decoder_output_file, decoder, shall_decode):
    click.echo(f"Allocator used: {allocator}")
    click.echo(f"Tracer library used: {tracer_lib}")
    prepare_environment(allocator, tracer_lib)

    old_trace = Path(trace_file)
    if old_trace.exists():
        if clear_previous_trace:
            click.echo(f"Remove old trace file: {old_trace}")
            old_trace.unlink()
        else:
            click.echo("Cowardly refuse to delete old trace file. Abort.")
            return

    return_code = run_command(command)
    click.echo(f"Program exited with code {return_code}")
    restore_environment()
    if shall_decode:
        click.echo(f'decoding...')
        subprocess.call([decoder, trace_file], stdout=decoder_output_file)

@main.command()
@click.option('-d', '--decoder', type=click.Path(exists=True,
                                                 readable=True,
                                                 dir_okay=False),
              default=default_decoder_path)
@click.option('-o', '--decoder-output-file', type=click.File(mode='w'),
              default="decoder.output")
def decode(decoder, decoder_output_file):
    click.echo(f'decoding...')
    subprocess.call([decoder, trace_file], stdout=decoder_output_file)
