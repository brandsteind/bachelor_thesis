#include <stdlib.h>
#include <stdio.h>

int main() {
  void* p = malloc(10);
  free(p);
  p = malloc(20);
  free(p);
  p = malloc(30);
  free(p);
  return 0;
}
