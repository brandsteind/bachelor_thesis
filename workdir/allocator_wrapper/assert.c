#include <stdlib.h>

#include "assert.h"

static size_t strlen(const char* str)
{
  size_t ret = 0;
  while(*str++)
    ret++;
  return ret;
}

static void assert_write(const char* msg)
{
  int output_handle = open(ASSERT_FILENAME,
			   O_WRONLY | O_CREAT | O_APPEND,
			   S_IWUSR | S_IRUSR | S_IRGRP | S_IROTH);
  write(output_handle, msg, strlen(msg));
  write(output_handle, "\n", 1);
  close(output_handle);
}

void wrapper_assert(bool cond, const char* fileline)
{
  if (!cond)
  {
    assert_write(fileline);
    exit(ASSERT_ERROR);
  }
  return;
}
