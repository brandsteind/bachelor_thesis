#ifndef ERRORS_H
#define ERRORS_H

#define WRAPPER_DLSYM_ERROR 101
#define WRAPPER_DLOPEN_ERROR 102
#define WRAPPER_WRONG_MODULE 103

#define WRAPPER_UNKNOWN_CALL_TYPE 104
#define WRAPPER_FAILED_TO_OPEN_OUTPUT 100
#define TRACER_FAIL_TO_OPEN_STATM 110
#define TRACER_READ_ERROR 111

#define ALLOCATOR_NO_MEM_AVAILABLE 50

#define ASSERT_ERROR 120
#endif
