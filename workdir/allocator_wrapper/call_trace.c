#include "call_trace.h"

static int output_handle;
static pthread_mutex_t trace_mutex = PTHREAD_MUTEX_INITIALIZER;
static sem_t *write_sem;

call_trace_entry_t trace_cache[TRACE_CACHE_SIZE];
bool flag = false;
size_t trace_curr_ind = 0;

typedef enum init_state_e {
  NOT_INITED,
  INIT_STARTED,
  INITED
} init_state_t;
static init_state_t trace_init_state = NOT_INITED;

pid_t gettid()
{
  return (pid_t)syscall(SYS_gettid);
}

size_t get_res_size()
{
  /* it consumes too much time */
  return 0;
  int statm = open("/proc/self/statm", O_RDONLY);
  ASSERT(statm != -1);
  size_t rss = 0;
  char curr;
  while (true)
  {
    if (read(statm, &curr, 1) == 0)
    {
      ASSERT(true);
    }
    if (curr == ' ')
      break;
  }

  while (true)
  {
    if (read(statm, &curr, 1) == 0)
    {
      ASSERT(true);
    }
    if (curr != ' ')
      rss = rss * 10 + (size_t)(curr - '0');
    else
      break;
  }
  close(statm);
  return rss;
}

__attribute__((constructor(101)))
void init_trace()
{
  if (output_handle != 0)
  {
    return;
  }
  trace_init_state = INIT_STARTED;
  write_sem = sem_open("wrapper_semaphore", O_CREAT, S_IWUSR | S_IRUSR | S_IRGRP | S_IROTH, 1);
  ASSERT(write_sem != SEM_FAILED);
  output_handle = open(TRACE_OUTPUT_FILENAME,
		       O_WRONLY | O_CREAT | O_APPEND,
		       S_IWUSR | S_IRUSR | S_IRGRP | S_IROTH);
  ASSERT(output_handle != -1);
  trace_init_state = INITED;
}

__attribute__((destructor))
void finalize_trace()
{
  trace_flush();
  /* what if other object's destructor calls a traced function? */
  close(output_handle);
}

void trace_flush()
{
  if (!output_handle)
    init_trace();
  sem_wait(write_sem);
  write(output_handle, trace_cache, sizeof(call_trace_entry_t) * trace_curr_ind);
  trace_curr_ind = 0;
  sem_post(write_sem);
}

#define COPY_TRACE_BODY(funcname, dest, src)				\
  call_trace_ ## funcname ## _t *ptr = (call_trace_ ## funcname ## _t *) (src);	\
  memcpy((dest), ptr, sizeof(call_trace_ ## funcname ## _t));

/* WARNING: mutexes are used only after initialization;
            if there is more then one thread before init done,
            there will be a mess in trace_cache;
*/
#define TRACE_WRITE(funcname, type, body_present, trace_body)		\
  if (trace_init_state == INITED)					\
    pthread_mutex_lock(&trace_mutex);					\
  call_trace_entry_t *entry = &trace_cache[trace_curr_ind++];		\
  entry->hdr.call_type = type;						\
  entry->hdr.pid = getpid();						\
  entry->hdr.tid = gettid();						\
  entry->hdr.has_body = body_present;					\
  if (body_present)							\
  {									\
    COPY_TRACE_BODY(funcname, &entry->body. funcname, trace_body);	\
  }									\
  clock_gettime(CLOCK_THREAD_CPUTIME_ID, &entry->hdr.time);		\
  clock_gettime(CLOCK_MONOTONIC_RAW, &entry->hdr.wall_time);		\
  if (trace_init_state != INIT_STARTED					\
      && body_present							\
      && trace_curr_ind >= TRACE_CACHE_SIZE / 2)			\
  {									\
    trace_flush();							\
  }									\
  if (trace_init_state == INITED)					\
    pthread_mutex_unlock(&trace_mutex);					\

void trace_entry(call_type_t type)
{
  switch(type)
  {
  case CALL_TYPE_MALLOC:
  {
    TRACE_WRITE(malloc, type, false, NULL);
    break;
  }
  case CALL_TYPE_FREE:
  {
    TRACE_WRITE(free, type, false, NULL);
    break;
  }
  case CALL_TYPE_CALLOC:
  {
    TRACE_WRITE(calloc, type, false, NULL);
    break;
  }
  case CALL_TYPE_REALLOC:
  {
    TRACE_WRITE(realloc, type, false, NULL);
    break;
  }
  case CALL_TYPE_REALLOCARRAY:
  {
    TRACE_WRITE(reallocarray, type, false, NULL);
    break;
  }
  case CALL_TYPE_PTHREAD_CREATE:
  {
    TRACE_WRITE(pthread_create, type, false, NULL);
    break;
  }
#if 0
  case CALL_TYPE_BRK:
  {
    TRACE_WRITE(brk, type, false, NULL);
    break;
  }
  case CALL_TYPE_SBRK:
  {
    TRACE_WRITE(sbrk, type, false, NULL);
    break;
  }
  case CALL_TYPE_MMAP:
  {
    TRACE_WRITE(mmap, type, false, NULL);
    break;
  }
  case CALL_TYPE_MUNMAP:
  {
    TRACE_WRITE(munmap, type, false, NULL);
    break;
  }
#endif
  default:
    ASSERT(true);
  }
}

void trace_malloc(size_t size, void* ret)
{
  call_trace_malloc_t trace;
  trace.size = size;
  trace.ret = ret;
  TRACE_WRITE(malloc, CALL_TYPE_MALLOC, true, &trace);
}

void trace_free(void* ptr)
{
  call_trace_free_t trace;
  trace.ptr = ptr;
  TRACE_WRITE(free, CALL_TYPE_FREE, true, &trace);
}

void trace_calloc(size_t nmemb, size_t size, void* ret)
{
  call_trace_calloc_t trace;
  trace.nmemb = nmemb;
  trace.size = size;
  trace.ret = ret;
  TRACE_WRITE(calloc, CALL_TYPE_CALLOC, true, &trace);
}

void trace_realloc(void* ptr, size_t size, void* ret)
{
  call_trace_realloc_t trace;
  trace.ptr = ptr;
  trace.size = size;
  trace.ret = ret;
  TRACE_WRITE(realloc, CALL_TYPE_REALLOC, true, &trace);
}

void trace_reallocarray(void* ptr, size_t nmemb, size_t size, void* ret)
{
  call_trace_reallocarray_t trace;
  trace.ptr = ptr;
  trace.nmemb = nmemb;
  trace.size = size;
  trace.ret = ret;
  TRACE_WRITE(reallocarray, CALL_TYPE_REALLOCARRAY, true, &trace);
}

void trace_pthread_create(pthread_t *thread, const pthread_attr_t *addr,
			  void* (*start_routine) (void*), void *arg, int ret)
{
  (void)thread;
  (void)addr;
  (void)start_routine;
  (void)arg;
  call_trace_pthread_create_t trace;
  trace.ret = ret;
  TRACE_WRITE(pthread_create, CALL_TYPE_PTHREAD_CREATE, true, &trace);
}

void trace_brk(void* addr, void* ret)
{
  call_trace_brk_t trace;
  trace.addr = addr;
  trace.ret = ret;
  TRACE_WRITE(brk, CALL_TYPE_BRK, true, &trace);
}

void trace_mmap(void* addr, size_t len, int prot, int flags, int fildes, off_t off, void* ret)
{
  call_trace_mmap_t trace;
  trace.addr = addr;
  trace.len = len;
  trace.prot = prot;
  trace.flags = flags;
  trace.fildes = fildes;
  trace.off = off;
  trace.ret = ret;
  TRACE_WRITE(mmap, CALL_TYPE_MMAP, true, &trace);
}

void trace_munmap(void* addr, size_t len, int ret)
{
  call_trace_munmap_t trace;
  trace.addr = addr;
  trace.len = len;
  trace.ret = ret;
  TRACE_WRITE(munmap, CALL_TYPE_MUNMAP, true, &trace);
}
