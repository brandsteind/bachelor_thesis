import sys

class Variable:
    def __init__(self, name, type, value = None):
        self.name = name
        self.type = type;
        self.value = value

class Func:
    def __init__(self, name):
        self.name = name
        self.code = []

class Program:
    def __init__(self):
        self.includes = []
        self.global_vars = dict() # name - Variable mapping
        self.value_name_map = dict() # value - name mapping
        self.main_init = []
        self.main = []
        self.thread_funcs = dict() # tid - Func mapping
        self.new_thread_funcs = []
        self._var_name_index = 0
        self._thread_name_index = 0
        self._thread_init_num = 0

    def get_new_var_name(self):
        self._var_name_index += 1
        return f"global_var{self._var_name_index}"

    def get_new_thread_func(self):
       self._thread_name_index += 1
       return Func(f"thread_main{self._thread_name_index}")

    def add_include(self, include):
        self.includes.append(include)

    def set_main_thread_tid(self, tid):
        self.main_thread_tid = tid

    def add_sleep(self, tid, time):
        #TODO: handle sleep interruptions
        var_name = self.get_new_var_name()
        seconds, nseconds = f"{time:.9f}".split('.')
        self.global_vars[var_name] = Variable(var_name,
                                              "struct_timespec",
                                              f"{{ .tv_sec = {seconds}, .tv_nsec = {nseconds:.9} }}")

        if tid == self.main_thread_tid:
            self.main.append(f"nanosleep(&{var_name}, NULL);")
        else:
            self.thread_funcs.code.append(f"nanosleep(&{var_name}, NULL);")

    def add_barriers(self):
        var_name = self.get_new_var_name()
        thread_count = self._thread_init_num + 1
        self.global_vars[var_name] = Variable(var_name,
                                              "pthread_barrier_t")
        self.main_init.append(f"pthread_barrier_init(&{var_name}, NULL, {thread_count});")
        self.main.append(f"pthread_barrier_wait(&{var_name});")
        for thread in self.thread_funcs.values():
            thread.code.append(f"pthread_barrier_wait(&{var_name});")
        for thread in self.new_thread_funcs:
            thread.code.append(f"pthread_barrier_wait(&{var_name});")


    def add_call(self, tid, call):
        if tid != self.main_thread_tid and not tid in self.thread_funcs.keys():
            assert len(self.new_thread_funcs) > 0
            self.thread_funcs[tid] = self.new_thread_funcs.pop()
        if call.name == "pthread_create":
            new_thread = self.get_new_thread_func()
            self.new_thread_funcs.append(new_thread)
            thread_num = self._thread_init_num
            self._thread_init_num += 1
            if tid == self.main_thread_tid:
                self.main.append(f"pthread_create(&threads[{thread_num}], NULL, {new_thread.name}, NULL);")
            else:
                self.thread_funcs[tid].code.append(f"pthread_create(&threads[{thread_num}], NULL, {new_thread.name}, NULL);")
        else:
            if tid == self.main_thread_tid:
                code = self.main
            else:
                code = self.thread_funcs[tid].code
            if call.name == "malloc":
                self.add_malloc(code, call)
            elif call.name == "calloc":
                self.add_calloc(code, call)
            elif call.name == "realloc":
                self.add_realloc(code, call)
            elif call.name == "free":
                self.add_free(code, call)
            else:
                assert False

    def add_malloc(self, code, call):
        var = self.get_new_var_name()
        self.global_vars[var] = Variable(var, "void*")
        code.append(f"{var} = malloc({call.params[0]});")
        self.value_name_map[call.ret] = var

    def add_calloc(self, code, call):
        var = self.get_new_var_name()
        self.global_vars[var] = Variable(var, "void*")
        code.append(f"{var} = calloc({call.params[0]}, {call.params[1]});")
        self.value_name_map[call.ret] = var

    def add_realloc(self, code, call):
        var = self.get_new_var_name()
        self.global_vars[var] = Variable(var, "void*")
        if (call.params[0] == '(nil)'):
            param1 = 'NULL'
        else:
            param1 = self.value_name_map[call.params[0]]
        code.append(f"{var} = realloc({param1}, {call.params[1]});")
        self.value_name_map[call.ret] = var

    def add_free(self, code, call):
        if call.params[0] == '(nil)':
            param = 'NULL'
        else:
            param = self.value_name_map.get(call.params[0])
            if param is None:
                print(f"incorrect free: {call.params[0]}", file=sys.stdout)
                return
            else:
                del self.value_name_map[call.params[0]]
        code.append(f"free({param});")


    def emit_includes(self):
        ret = ""

        for include in self.includes:
            ret += "#include <{}>\n".format(include)
        ret += "\n"
        return ret

    def emit_global_vars(self):
        ret = ""
        for var in self.global_vars.values():
            ret += f"{var.type} {var.name}"
            if var.value != None:
                ret += " = {};\n".format(var.value)
            else:
                ret += ";\n"
        ret += "\n"
        return ret

    def emit_thread_funcs(self):
        ret = ""

        threads = list(self.thread_funcs.values())
        threads.extend(self.new_thread_funcs)

        for thread in threads:
            ret += "void* {} (void *arg)\n{{\n".format(thread.name)
            for line in thread.code:
                ret += "    {}\n".format(line)
            ret += "}\n\n"

        return ret

    def emit_main(self):
        ret = ""

        ret += "int main()\n{\n"

        for line in self.main_init:
            ret += "    {}\n".format(line)
        ret += "\n"

        for line in self.main:
            ret += "    {}\n".format(line)


        if len(self.thread_funcs) > 0:
            ret += "    for (size_t i = 0; i < {}; i++)\n".format(len(self.thread_funcs))
            ret += "    {\n        pthread_join(threads[i], NULL);\n    }\n\n"

        ret += "}\n\n"

        return ret

    def emit(self):
        thread_count = len(self.thread_funcs)
        self.global_vars["threads"] = Variable(f"threads[{thread_count}]",
                                               "pthread_t")

        return "".join([self.emit_includes(),
                        self.emit_global_vars(),
                        self.emit_thread_funcs(),
                        self.emit_main()])

class CodeBuilder:
    def __init__(self):
        self.program = Program()
        self.threads = set()
        self.avail_new_threads = 0
        self.is_multithreaded = False
        self.curr_wall_time = None
        self.trace = None
        self.initialized = False
        self.is_all_calls_processed = False
        self.program.add_include("stdlib.h")
        self.program.add_include("time.h")
        self.program.add_include("pthread.h")


    def initialize(self):
        assert not self.initialized
        assert self.trace
        self.initialized = True
        self.indexes = dict()
        for thread in self.trace:
            self.indexes[thread] = 0
        if len(self.trace) > 1:
            self.is_multithreaded = True

        first_thread, first_call = self.get_earliest_call(False)
        self.threads.add(first_thread)
        self.program.set_main_thread_tid(first_thread)
        self.curr_wall_time = first_call.info.wall_time_start

    def get_earliest_call(self, advance_index = True):
        min_time = None
        curr_thread = None
        for thread in self.indexes:
            first_wall_time = self.trace[thread][self.indexes[thread]].info.wall_time_start
            if min_time is None or min_time > first_wall_time:
                min_time = first_wall_time
                curr_thread = thread
        call = self.trace[curr_thread][self.indexes[curr_thread]]
        if advance_index:
            self.indexes[curr_thread] += 1
            if len(self.trace[curr_thread]) == self.indexes[curr_thread]:
                del self.indexes[curr_thread]
        if len(self.indexes) == 0:
            self.is_all_calls_processed = True
        return curr_thread, call

    def gen(self, trace, preserve_timings = False):
        self.trace = trace
        self.initialize()
        while not self.is_all_calls_processed:
            tid, call = self.get_earliest_call()
            if not tid in self.threads:
                assert self.avail_new_threads > 0
                self.avail_new_threads -= 1
                self.threads.add(tid)
            if preserve_timings:
                self.program.add_sleep(tid, call.info.wall_time_ret - self.curr_wall_time)
            self.curr_wall_time = call.info.wall_time_ret
            if self.is_multithreaded:
                self.program.add_barriers()
            if call.name == "pthread_create":
                self.avail_new_threads += 1
            self.program.add_call(tid, call)

        return self.program.emit()
