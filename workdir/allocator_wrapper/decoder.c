#include <stdio.h>

#include "call_trace.h"

void parse_log(const char* path)
{
  call_trace_entry_t entry;
  FILE* file = fopen(path, "r");
  if (file == NULL)
  {
    printf("error on open\n");
    return;
  }
  while(fread(&entry, sizeof(call_trace_entry_t), 1, file))
  {
    printf("(pid %x) ", entry.hdr.pid);
    printf("(tid %x) ", entry.hdr.tid);
    printf("[%ld.%09ld] ", entry.hdr.time.tv_sec, entry.hdr.time.tv_nsec);
    printf("{%ld.%09ld} ", entry.hdr.wall_time.tv_sec, entry.hdr.wall_time.tv_nsec);

    if (!entry.hdr.has_body)
    {
      switch(entry.hdr.call_type)
      {
      case CALL_TYPE_MALLOC:
	printf("call malloc\n");
	break;
      case CALL_TYPE_FREE:
	printf("call free\n");
	break;
      case CALL_TYPE_CALLOC:
	printf("call calloc\n");
	break;
      case CALL_TYPE_REALLOC:
	printf("call realloc\n");
	break;
      case CALL_TYPE_REALLOCARRAY:
	printf("call reallocarray\n");
	break;
      case CALL_TYPE_PTHREAD_CREATE:
	printf("call pthread_create\n");
	break;
      case CALL_TYPE_BRK:
	printf("call brk\n");
	break;
      case CALL_TYPE_MMAP:
	printf("call mmap\n");
	break;
      case CALL_TYPE_MUNMAP:
	printf("call munmap\n");
	break;
      default:
	printf("ERROR!\n");
	break;
      }
    }
    else
    {
      switch(entry.hdr.call_type)
      {
      case CALL_TYPE_MALLOC:
      {
	printf("malloc(%ld) -> %p\n",
	       entry.body.malloc.size,
	       entry.body.malloc.ret);
	break;
      }
      case CALL_TYPE_FREE:
      {
	printf("free(%p)\n", entry.body.free.ptr);
	break;
      }
      case CALL_TYPE_CALLOC:
      {
	printf("calloc(%ld, %ld) -> %p\n",
	       entry.body.calloc.nmemb,
	       entry.body.calloc.size,
	       entry.body.calloc.ret);
	break;
      }
      case CALL_TYPE_REALLOC:
      {
	printf("realloc(%p, %ld) -> %p\n",
	       entry.body.realloc.ptr,
	       entry.body.realloc.size,
	       entry.body.realloc.ret);
	break;
      }
      case CALL_TYPE_REALLOCARRAY:
      {
	printf("reallocarray(%p, %ld, %ld) -> %p\n",
	       entry.body.reallocarray.ptr,
	       entry.body.reallocarray.nmemb,
	       entry.body.reallocarray.size,
	       entry.body.reallocarray.ret);
	break;
      }
      case CALL_TYPE_PTHREAD_CREATE:
      {
	printf("pthread_create() -> %d\n",
	       entry.body.pthread_create.ret);
	break;
      }
      case CALL_TYPE_BRK:
      {
	printf("brk(%p) -> %p\n",
	       entry.body.brk.addr,
	       entry.body.brk.ret);
	break;
      }
      case CALL_TYPE_MMAP:
      {
	printf("mmap(%p, %ld, %d, %d, %d, %ld) -> %p\n",
	       entry.body.mmap.addr,
	       entry.body.mmap.len,
	       entry.body.mmap.prot,
	       entry.body.mmap.flags,
	       entry.body.mmap.fildes,
	       entry.body.mmap.off,
	       entry.body.mmap.ret);
	break;
      }
      case CALL_TYPE_MUNMAP:
      {
	printf("munmap(%p, %ld) -> %d\n",
	       entry.body.munmap.addr,
	       entry.body.munmap.len,
	       entry.body.munmap.ret);
	break;
      }
      default:
	printf("ERROR!\n");
	break;
      }
    }
  }
}

int main(int argc, char** argv)
{
  if (argc == 2)
  {
    parse_log(argv[1]);
  }
  return 0;
}
